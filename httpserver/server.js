const express = require('express')
const app = express();
const port = 8080
var fs = require('fs');
var amqp = require('amqplib/callback_api');
const bodyparser = require('body-parser');
const url = 'amqp://rabbitmq';
const queue = "state.sender"
let STATE = "RUNNING";
const FILELOCATIONLOGS = "/var/lib/observer/data/logs.txt";
const FILELOCATIONMESSAGES = "/var/lib/observer/data/messages.txt";
const LOCALDEV=true;
const request = require('request');

app.use(bodyparser.text());
//GET the messages.txt file and return it as a HTTP Response
app.get('/messages', (req, res) => {
  res.setHeader('content-type', 'text/plain');
  fs.readFile(FILELOCATIONMESSAGES, 'utf8', (err, data) => {
    if (err) {
      console.error(err);
      res.send("Error reading files");
    }
    res.send(data)
  });
  
})

app.put('/state', async (req, res) => {
  const newState = req.body;
  res.setHeader('content-type', 'text/plain');
  if(newState != STATE && newState.match(/^(INIT|PAUSED|RUNNING|SHUTDOWN)$/)) {
    STATE = newState;
    const timestamp = new Date().toISOString();
    const result = `${timestamp}: ${STATE} \n`;
    fs.writeFileSync(FILELOCATIONLOGS, result,{flag: "a"});
    await sendState(STATE);
    if(STATE == "SHUTDOWN") {
      console.log("Shutting down httpserver")
      res.status(200).send("Shutting down containers")
     // process.exit(128 + "SIGNTERM");
    } else {
      if(STATE == "INIT") {
        STATE = "RUNNING";
      }
      res.status(200).send(newState);
    }
    
  } else {
    res.status(403).send("State active already or false input")
  }
  
  
})

app.get('/state', (req, res) => {
  res.setHeader('content-type', 'text/plain');
  res.status(200).send(STATE);
})

app.get('/run-log', (req, res) => {
  res.setHeader('content-type', 'text/plain');
  fs.readFile(FILELOCATIONLOGS, 'utf8', (err, data) => {
    if (err) {
      console.error(err);
      res.status(500).send("Error reading files");
    } else {
      res.status(200).send(data)
    }
   
  });
  
})

app.get('/node-statistic', async (req, res) => {

  const options = {
    url: 'http://rabbitmq:15672/api/nodes',
    auth: {
      user: "guest",
      password: "guest"
    },
    json:true
  }
  res.setHeader('content-type', 'application/json');

  await request(options, (err, resp, body) => {
    if (err) { resp.status(500).send("Error getting data"); }
    res.status(200).send({
      sockets: body[0].sockets_total,
      mem_limit: body[0].mem_limit,
      uptime: body[0].uptime,
      running: body[0].running,
      contexts: body[0].contexts
    })
  });
});

app.listen(port, () => {

  console.log(`Example app listening on port ${port}`)
})


async function sendState(state) {
  let channel = null;
  amqp.connect(url, function (err, conn) {

      if (err) {
          console.log(err);
          throw new Error(`AMQP connection not available on ${url}`);
      }
      conn.createChannel(async function (err, ch) {

          channel = ch;
          channel.assertExchange(queue, "topic",{
              durable: false
            });
              console.log("Sending state " + state);
              channel.publish(queue,"state.sender", Buffer.from(state));
              await timeout(3000);
              if(state == "SHUTDOWN") {
                process.exit(128+"SIGNTERM");
              }
              return;
      });
  })
}
function timeout(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
module.exports = app;