var amqp = require('amqplib/callback_api');
const url = 'amqp://rabbitmq';
const queue = 'compse140.o';
const receivequeue = "state.sender"
let STATE = "RUNNING";
let channel = null;
amqp.connect(url, function (err, conn) {

    if (err) {
        console.log(err);
        throw new Error(`AMQP connection not available on ${url}`);
    }
    conn.createChannel(async function (err, ch) {

        channel = ch;
        channel.assertExchange(queue, "topic",{
            durable: false
          });

        channel.assertExchange(receivequeue, "topic", {
            durable: false
          });

          channel.assertQueue('', {
            exclusive:true
        }, function(error2, q){
            if(error2) {
                throw error2
            }
            //Subscribe to topic compse140.o
            channel.bindQueue(q.queue, receivequeue, "state.sender");
            channel.consume(q.queue, async function(msg) {
                await timeout(1000);
                if(msg.content.toString() == "SHUTDOWN") {
                    console.log("Shutting down orig");
                    try {
                        channel.publish(queue,"compse140.o", Buffer.from("SHUTDOWN"));
                        await timeout(2000);
                        await channel.close();
                        await conn.close();
                        await timeout(2000);
                        process.exit(128+"SIGNTERM")
                    } catch(e) {
                        console.log(e)
                    }
                }
                if(msg.content.toString() == "RESET") {
                    channel.publish(queue,"compse140.o", Buffer.from("RESET"));
                } 
                
                if(msg.content.toString() == "INIT") {
                    counter = 1;
                    STATE = "RUNNING";
                    sendMessages(channel,counter);
                } else if(msg.content.toString() == "RUNNING"){
                    STATE = msg.content.toString();
                    sendMessages(channel,counter);
                } else if(msg.content.toString() == "PAUSED") {
                    STATE = msg.content.toString();
                }
              }, {
                noAck: true
              });
        });

        counter = 1;
        while(STATE == "RUNNING") {
            await timeout(3000)
            channel.publish(queue,"compse140.o", Buffer.from("MSG_" + counter));
            console.log("MSG_" + counter);
            counter++;
            
        } 
    });
})

process.on('exit', code => {
    channel.close();
    console.log(`Closing`);
  });


function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function sendMessages(channel, counter) {
        while(STATE == "RUNNING") {
            await timeout(3000)
            channel.publish(queue,"compse140.o", Buffer.from("MSG_" + counter));
            console.log("MSG_" + counter);
            counter++;
            
        } 
}
