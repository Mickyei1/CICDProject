const express = require('express');
const chai = require('chai');
const request = require('supertest');
const app = express();
const axios = require('axios');
var expect = chai.expect;

describe('API testing', () => {
 it('GET /messages', async () => {
  await axios
    .get('http://localhost:8080/messages', "", {headers: {"Content-type": "text/plain"}})
    .then((res) => {
      expect(res).to.have.property("status");
      expect(res.status).to.equal(200);
    }).catch((err) => {
      expect(err).to.be.null;
    });
  });

  it('PUT /state PAUSED', async () => {
    await axios
    .put('http://localhost:8080/state', "PAUSED", {headers: {"Content-type": "text/plain"}})
    .then((res) => {
      expect(res).to.have.property("status")
      expect(res.status).to.equal(200)
    }).catch((err) => {
        expect(err).to.be.null;
    });
  });

  it('PUT /state INIT', async () => {
    await axios
    .put('http://localhost:8080/state', "INIT", {headers: {"Content-type": "text/plain"}})
    .then((res) => {
      expect(res).to.have.property("status")
      expect(res.status).to.equal(200)
    }).catch((err) => {
      expect(err).to.be.null;
    });
  });

  it('GET /state', async () => {
    await axios
      .get('http://localhost:8080/state', "", {headers: {"Content-type": "text/plain"}})
      .then((res) => {
        expect(res).to.have.property("status")
        expect(res.status).to.equal(200)
        expect(res.data).to.equal("RUNNING")
      }).catch((err) => {
      });
    });

    it('GET /run-log', async () => {
      await axios
        .get('http://localhost:8080/run-log', "", {headers: {"Content-type": "text/plain"}})
        .then((res) => {
          expect(res).to.have.property("status")
          expect(res.status).to.equal(200)
        }).catch((err) => {
          expect(err).to.be.null;
        });
      });

      it('PUT /state SHUTDOWN', async () => {
        await axios
        .put('http://localhost:8080/state', "SHUTDOWN", {headers: {"Content-type": "text/plain"}})
        .then((res) => {
          expect(res).to.have.property("status")
          expect(res.status).to.equal(200)
        }).catch((err) => {
            expect(err).to.be.null;
        });
      });

});

function timeout(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}